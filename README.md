# Dark UserStyles #

This is just an informal grouping of some userstyles I use with the Stylus web extension to create a "night mode" -type feeling across the websites I frequent.
Feel free to use or modify them however you see fit.